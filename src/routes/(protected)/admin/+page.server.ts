import { redirect } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async ({ locals }) => {
	// перенаправить пользователя, если он не залогинен
	if (!locals.user) {
		throw redirect(302, '/');
	}
};
