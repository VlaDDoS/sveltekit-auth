import { fail, redirect } from '@sveltejs/kit';
import type { Action, Actions, PageServerLoad } from './$types';

import bcrypt from 'bcrypt';
import { db } from '$lib/db';

export const load: PageServerLoad = async ({ locals }) => {
	// перенаправить пользователя, если он залогинен
	if (locals.user) {
		throw redirect(302, '/');
	}
};

const login: Action = async ({ cookies, request }) => {
	const data = await request.formData();
	const username = data.get('username');
	const password = data.get('password');

	if (typeof username !== 'string' || typeof password !== 'string' || !username || !password) {
		return fail(400, { invalid: true });
	}

	const user = await db.user.findUnique({
		where: { username }
	});

	if (!user) {
		return fail(400, { credentials: true });
	}

	const userPassword = await bcrypt.compare(password, user.passwordHash);

	if (!userPassword) {
		return fail(400, { credentials: true });
	}

	// генерим новый токен
	const authenticatedUser = await db.user.update({
		where: { username: user.username },
		data: { userAuthToken: crypto.randomUUID() }
	});

	cookies.set('session', authenticatedUser.userAuthToken, {
		// отправляем cookie на каждую страницу
		path: '/',

		// cookie только на стороне сервера
		httpOnly: true,

		// только запросы одного и того же сайта, могут отправлять cookie
		sameSite: 'strict',

		// работать только на HTTPS в prod
		secure: process.env.NODE_ENV === 'production',

		// устанавливаем cookie только на месяц
		maxAge: 60 * 60 * 24 * 30
	});

	// перенаправляем пользователя на главную страницу
	throw redirect(302, '/');
};

export const actions: Actions = { login };
