import type { Handle } from '@sveltejs/kit';
import { db } from '$lib/db';

export const handle: Handle = async ({ event, resolve }) => {
	// получить cookie из браузера
	const session = event.cookies.get('session');

	if (!session) {
		return await resolve(event);
	}

	// найти пользователя на основе сеанса
	const user = await db.user.findUnique({
		where: { userAuthToken: session },
		select: { username: true, role: true }
	});

	// если пользователь существует установи events.locale
	if (user) {
		event.locals.user = {
			name: user.username,
			role: user.role.name
		};
	}

	// загрузить страницу как обычно
	return await resolve(event);
};
